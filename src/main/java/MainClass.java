import ua.com.annotation.annoclass.FirstAnnClass;
import ua.com.annotation.annointerface.CarAnnotation;

import java.lang.reflect.Field;

public class MainClass {
    public static void main(String[] args) {

        FirstAnnClass firstAnnClass = new FirstAnnClass(32, 3245, "321E");

        Class clazz = FirstAnnClass.class;
        Field[] field = clazz.getDeclaredFields();

        for (Field fields : field) {
            if (fields.isAnnotationPresent(CarAnnotation.class)) {
                CarAnnotation carAnnotation = (CarAnnotation) fields.getAnnotation(CarAnnotation.class);
                String name = carAnnotation.name();
                String country = carAnnotation.country();
                System.out.println("GetName +" + fields.getName());
                System.out.println("NAme +" + name);
                System.out.println("Country +" + country);
            }
        }
    }
}
