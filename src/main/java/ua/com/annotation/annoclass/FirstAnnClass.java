package ua.com.annotation.annoclass;

import ua.com.annotation.annointerface.CarAnnotation;

//@CarAnnotation(name = "Some Name Car", country = "Same Country")
public class FirstAnnClass {

    @CarAnnotation(name = "Some Car", country = "Germany")
    private String name;
    @CarAnnotation(name = "Aston", country = "GB")
    private String country;
    private int age;
    private int run;
    private String model;

//    @CarAnnotation(name = "BMW", country = "Germany")
    public FirstAnnClass(int age, int run, String model) {
        this.age = age;
        this.run = run;
        this.model = model;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getRun() {
        return run;
    }

    public void setRun(int run) {
        this.run = run;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return "FirstAnnClass{" +
                "name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", age=" + age +
                ", run=" + run +
                ", model='" + model + '\'' +
                '}';
    }
}
